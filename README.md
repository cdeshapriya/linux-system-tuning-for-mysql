# Linux System Tuning For MySQL

# Environment

CentOS 7, Ubuntu 18.04/20.04

# Reasons for Tuning 

* High memory consumption on MySQL System
* High CPU consumption on host system
* How to configure swap memory for a mySQL database
* Slower than expected IO performance running MySQL on Linux

## Kernel – vm.swappiness

The value represents the tendency of the kernel to swap out memory pages. On a database server with ample amounts of RAM, we should keep this value as low as possible. The extra I/O can slow down or even render the service unresponsive. A value of 0 disables swapping completely while 1 causes the kernel to perform the minimum amount of swapping. In most cases the latter setting should be OK. 

Swapping is not ideal for databases and should be avoided as much as possible.

The change should be also persisted in  **/etc/sysctl.conf**:

Add the following entry to **/etc/sysctl.conf**

```
vm.swappiness = 1
```

## The Page Cache

In computing, a page cache, sometimes called a disk cache, is a transparent cache for the pages originating from a secondary storage device, such as a hard disk drive (HDD) or a solid-state drive (SSD). The operating system keeps a page cache in otherwise unused portions of the main memory (RAM), resulting in quicker access to the contents of cache pages and overall performance improvements. A page cache is implemented in kernels with the paging memory management and is mostly transparent to applications.

## How to identify the Linux page cache size

Under Linux, you can check the page cache size via the command **free -w -h** The page cache size is indicated in the cache column as depicted in the following figure. 

```
free -w -h
```
![free -w -h](images/mem-summary.png "How to identify the Linux page cache size")

## How Page Cache is Generated

### Writing

If data is written, it is first written to the page cache, where it is managed as one of the dirty pages. **_The term "dirty" refers to data that is stored in the page cache but must first be written to the underlying storage device_**. The content of these dirty pages is transferred to the underlying storage device on a regular basis (via system calls such as **sync** or **fsync**). In this case, the system could be a RAID controller or the hard disk itself.

### Reading

File blocks are written to the page cache not only when they are written, but also when they are read. For instance, if you read a 100-megabyte file twice, once after the other, the second access is faster. This is because the file blocks are read directly from memory's page cache and do not need to be read from the hard disk again.

### Why is it not released in time? 

The Linux kernel provides the page cache mechanism. By occupying unused RAM with caches, the Linux kernel attempts to optimize RAM utilization.

This is done under the assumption that unused RAM is wasted RAM. The page cache is not released until there is insufficient memory to do so.

### How to clear page cache

**By command**

```
echo 1 > /proc/sys/vm/drop_caches
```

**By script**

```
#
# This script checks the page cache every [delay] seconds
# If the page cache is greater than or equal to [limit],
# then this script will clear the page cache
#!/bin/bash
delay=$1
limit=$2

[ "$#" -lt 2 ] && echo "Usage cacheDrop.sh [delay time sec] [cache size GB]" && exit 0

while true;
do
   cache=$(free -w -g | grep Mem | awk '{ print $7 }')
   if [ "$cache" -ge $limit ]; then
      echo 1 > /proc/sys/vm/drop_caches
   fi
done

sleep $delay  
```

### Risks from clearing page cache

Clearing cache will free RAM, but it causes the kernel to look for files on the disk, rather than in the cache. This can cause performance issues.

Normally, the kernel will clear the cache when the available RAM is depleted. It frequently writes dirtied content to the disk using **pdflush**. 

### The impact of too much Linux page cache

The Linux kernel attempts to optimize RAM utilization by occupying unused RAM with caches. This is done on the basis that unused RAM is wasted RAM.

Over time, the kernel will fill the RAM with the cache. As more memory is required by the applications/buffers, the kernel goes through the cache memory pages and finds a block large enough to fit the requested **malloc**. It then frees that memory and allocates it to the calling application.

Under some circumstances, this can affect the general performance of the system as cache de-allocation is time consuming compared to accessing unused RAM. Higher latency can therefore sometimes be observed.

This latency will be based on the fact that RAM is being used to its full speed potential. No other symptoms may occur apart from overall and potentially sporadic latency increases. The equivalent is similar to symptoms that may be observed if the hard disks are not keeping up with reads and writes. The latency may also affect either Aerospike or operating system components, such as network cards, iptables, and iproute2 mallocs. This may show network-based latency instead. 

### What is the impact?

A disk cache is a software mechanism that allows the system to keep in the RAM some data that is normally stored on a disk, so that further access to that data can be satisfied quickly without accessing the disk. 

Disk caches are crucial for system performance, because repeated access to the same disk data is common. A user mode process that interacts with a disk is entitled to ask repeatedly to read or write the same disk data. 

Furthermore, different processes may also need to address the same disk data at different times. For example, you may use the **cp** command to copy a text file, and then invoke your preferred **editor **to modify it. 

To satisfy your request, the command shell will create two different processes that access the same file at different times.

### What Tunings Can Do?

We can do the tuning to kernel parameters that come as **vm_dirty **in Linux. To find out  the current setting execute the following command.

```
$ sysctl -a | grep dirty
 vm.dirty_background_ratio = 10
 vm.dirty_background_bytes = 0
 vm.dirty_ratio = 20
 vm.dirty_bytes = 0
 vm.dirty_writeback_centisecs = 500
 vm.dirty_expire_centisecs = 3000
```

#### vm.dirty_background_ratio 

It is the percentage of system memory that can be filled with “dirty” pages  memory pages that still need to be written to disk before the pdflush/flush/kdmflush background processes kick in to write it to disk. As per the above example is 10%, so if your virtual server has 32 GB of memory that’s 3.2 GB of data that can be sitting in RAM before something is done.

Add the following entry to **/etc/sysctl.conf**

```
vm.dirty_background_ratio = 10
```

#### **vm.dirty_ratio** 


It is the absolute maximum amount of system memory that can be filled with dirty pages before everything must get committed to disk. When the system gets to this point all new I/O blocks until dirty pages have been written to disk. This is often the source of long I/O pauses, but is a safeguard against too much data being cached unsafely in memory.

**vm.dirty_background_bytes** and **vm.dirty_bytes** are another way to specify these parameters. If you set the _bytes version the _ratio version will become 0, and vice-versa.

Add the following entry to **/etc/sysctl.conf**

```
vm.dirty_ratio = 40
```
#### vm.dirty_expire_centisecs 

It is how long something can be in cache before it needs to be written. In this case it’s 30 seconds as per the example above. When the **pdflush/flush/kdmflush** processes kick in they will check to see how old a dirty page is, and if it’s older than this value it’ll be written asynchronously to disk. Since holding a dirty page in memory is unsafe this is also a safeguard against data loss.

Add the following entry to **/etc/sysctl.conf**

```
vm.dirty_expire_centisecs = 500
```

#### vm.dirty_writeback_centisecs

 is how often the **pdflush/flush/kdmflush** processes wake up and check to see if work needs to be done.

You can also see statistics on the page cache in **/proc/vmstat**:

An example is below. 

```
cat /proc/vmstat | egrep "dirty|writeback"
nr_dirty 382
nr_writeback 0
nr_writeback_temp 0
nr_dirty_threshold 1178793
nr_dirty_background_threshold 588677
```

Add the following entry to **/etc/sysctl.conf**

```
vm.dirty_writeback_centisecs = 100
```

#### Semaphores

Semaphore is simply a variable that is non-negative and shared between threads. This variable is used to solve the critical section problem and to achieve process synchronization in the multiprocessing environment.

Add the following entry to **/etc/sysctl.conf**

```
kernel.sem = 250 32000 100 128
```

* The first value, **SEMMSL**, is the maximum number of semaphores per semaphore set
* The second value, **SEMMNS**, defines the total number of semaphores for the system
* The third value, **SEMOPM**, defines the maximum number of semaphore operations per semaphore call
* The last value, **SEMMNI**, defines the number of entire semaphore sets for the system

### Open file descriptors for MySQL

On Linux, the set of file descriptors open in a process can be accessed under the path /proc/PID/fd/ , where PID is the process identifier. In Unix-like systems, file descriptors can refer to any Unix file type named in a file system. The following depicts a scenario.

![](images/linux-file-descripters.png "Linux open file descripters")

Add the following entry to **/etc/security/limits.conf**

```
#<domain>      <type>  <item>         <value>
mysql          hard    nofile          10000
```

## I/O Scheduler

The completely fair queue (CFQ) I/O scheduler, is the current default scheduler in the Linux kernel. It uses both request merging and elevators and is a bit more complex that the NOOP or deadline schedulers. CFQ synchronously puts requests from processes into a number of per-process queues then allocates time slices for each of the queues to access the disk. The details of the length of the time slice and the number of requests a queue is allowed to submit are all dependent on the I/O priority of the given process. Asynchronous requests for all processes are batched together into fewer queues with one per priority.

The default CFQ I/O scheduler is appropriate for most workloads, but does not offer optimal performance for database environments

* The **deadline** scheduler is recommended for physical systems
* The **noop** scheduler is recommended for virtual systems

### Configuring the I/O scheduler on Red Hat Enterprise Linux 7

The default scheduler in later Red Hat Enterprise Linux 7 versions (7.5 and later) is deadline.

To make the changes persistent through boot you have to add **elevator=noop** to **GRUB_CMDLINE_LINUX** in **/etc/default/grub** as shown below.

```
GRUB_CMDLINE_LINUX="crashkernel=auto rd.lvm.lv=vg00/lvroot rhgb quiet elevator=noop"
```

After the entry has been created/updated, rebuild the** /boot/grub2/grub.cfg** file to include the new configuration with the added parameter:

#### On BIOS-based machines:

```
grub2-mkconfig -o /boot/grub2/grub.cfg
```

#### On UEFI-based machines:


```
grub2-mkconfig -o /boot/efi/EFI/redhat/grub.cfg
```

## Tuning MySQL

### What is an InnoDB Buffer Pool?

InnoDB buffer pool is the memory space that holds many in-memory data structures of InnoDB, buffers, caches, indexes and even row-data. innodb_buffer_pool_size is the MySQL configuration parameter that specifies the amount of memory allocated to the InnoDB buffer pool by MySQL. This is one of the most important settings in the MySQL hosting configuration and should be configured based on the available system RAM.

We’ll walk you through two approaches of setting your InnoDB buffer pool size value, examine the pros and cons of those practices, and also propose a unique method to arrive at an optimum value based on the size of your system RAM.

### Approach 1. Rule of Thumb Method

The most commonly followed practice is to set this value at 70% – 80% of the system RAM. Though it works well in most cases, this method may not be optimal in all configurations. Let’s take the example of a system with 192GB of RAM. Based on the above method, we arrive at about 150GB for the buffer pool size. However, this isn’t really an optimal number as it does not fully leverage the large RAM size that’s available in the system, and leaves behind about 40GB of memory. This difference can be even more significant as we move to systems with larger configurations where we should be utilizing the available RAM to a greater extent.

### Approach 2. A More Nuanced Approach

This approach is based on a more detailed understanding of the internals of the InnoDB buffer pool and its interactions, which is described very well in the book High Performance MySQL.

Let’s look at the following method to compute the InnoDB buffer pool size.


* Start with total RAM available.
* Subtract suitable amount for the OS needs.
* Subtract suitable amount for all MySQL needs (like various MySQL buffers, temporary tables, connection pools, and replication related buffers).
* Divide the result by 105%, which is an approximation of the overhead required to manage the buffer pool itself.

#### An example system with 192GB RAM

For example, let’s look at a system with 192GB RAM using only InnoDB and having a total log file size of about 4GB. We can use a rule like ‘maximum of 2GB or 5% of total RAM’ for OS needs allocation as recommended in the above book, which comes to about 9.6GB. Then, we’ll also allocate about 4GB for other MySQL needs, mainly taking into account the log file size. This method results in about 170GB for our InnoDB buffer pool size, which is about 88.5% utilization of the available RAM size.

Though we used the ‘maximum of 2GB or 5% of total RAM’ rule to compute our memory allocation for OS needs above, the same rule does not work very well in all cases, specifically for systems with medium-sized RAMs between 2GB and 32GB. For instance, in a system with 3GB RAM, allocating 2GB for OS needs does not leave much for the InnoDB buffer pool, while allocating 5% of RAM is just too little for our OS needs.

So, let’s fine-tune the above OS allocation rule and examine the InnoDB computation method across various RAM configurations:

#### For Systems with Small-Sized RAM (&lt;= 1GB)

For systems running with less than 1GB of RAM, it is better to go with the MySQL default configuration value of 128MB for InnoDB buffer pool size.

#### For Systems with Medium-Sized RAM (1GB – 32GB)

Considering the case of systems with a RAM size of 1GB – 32GB, we can compute OS needs using this rough heuristics:

**256MB + 256 * log2(RAM size in GB)**

The rationalization here is that, for low RAM configurations, we start with a base value of 256MB for OS needs and increase this allocation in a logarithmic scale as the amount of RAM increases. This way, we can come up with a deterministic formula to allocate RAM for our OS needs. We’ll also allocate the same amount of memory for our MySQL other needs. For example, in a system with 3GB of RAM, we would make a fair allocation of 660MB for OS needs, and another 660MB for MySQL other needs, resulting in a value of about 1.6GB for our InnoDB buffer pool size.

#### For Systems with Higher-Sized RAM (> 32GB)

For systems with RAM sizes greater than 32GB, we would revert back to calculating OS needs as 5% of our system RAM size, and the same amount for MySQL other needs. So, for a system with a RAM size of 192GB, our method will land at about 165GB for InnoDB buffer pool size, which is again, an optimal value to be used.

### sort_buffer_size

The parameter sort_buffer_size is one of the MySQL parameters that is far from obvious to adjust. It is a per session buffer that is allocated every time it is needed. The problem with the sort buffer comes from the way Linux allocates memory.

Learn More at: [Impact of the sort buffer size in MySQL](https://www.percona.com/blog/2010/10/25/impact-of-the-sort-buffer-size-in-mysql/)

### key_buffer_size

key_buffer_size is a MyISAM variable which determines the size of the index buffers held in memory, which affects the speed of index reads. ... A good rule of thumb for servers consisting particularly of MyISAM tables is for about 25% or more of the available server memory to be dedicated to the key buffer.

### read_buffer_size 

read_buffer_size is also used to determine the memory block size for Memory tables. The read_rnd_buffer_size variable is also used mainly for MyISAM for reads from tables. Also, consider InnoDB or MariaDB's Aria storage engines. For the past decade the default values of these buffers have remained the same

### read_rnd_buffer_size

The read_rnd_buffer_size is used after a sort, when reading rows in sorted order. If you use many queries with ORDER BY, upping this can improve performance” which is cool but it does not really tell you how exactly read_rnd_buffer_size works as well as which layer it corresponds to – SQL or storage engine.

Learn more at: [What exactly is read_rnd_buffer_size](https://www.percona.com/blog/2007/07/24/what-exactly-is-read_rnd_buffer_size/)

#### Edit /etc/my.cnf  and adjust  the following parameters. 

```
innodb_buffer_pool_size  - 70% to 80% of main memory is adequate.
key_buffer_size  -  If you use MyISAM, approx 30% of main memory is adequate.
sort_buffer_size  -  256KB to 1MB
read_buffer_size  -  128KB to 512KB
read_rnd_buffer_size  -  256KB to 1MB
```
